# dockerizing a web app 

I am not using a taditional `dockerfile` but i will use a special implementation with advanced features like `multi-stage` building and static linking with `MUSL`

I make also a `gitlab-ci` file to make fully functional pipleine with a specifique logic 

here is the docker file :

```posh
 FROM rustdocker/rust:nightly as cargo-build 
 RUN apt-get update 
 RUN apt-get install musl-tools -y 
 RUN /root/.cargo/bin/rustup target add x86_64-unknown-linux-musl 
 RUN USER=root /root/.cargo/bin/cargo new --bin walk_away 
 WORKDIR /walk_away 
 COPY ./Cargo.toml ./Cargo.toml 
 COPY ./Cargo.lock ./Cargo.lock 
 ENV X86_64_UNKNOWN_LINUX_MUSL_OPENSSL_LIB_DIR=/usr/lib/x86_64-linux-gnu/ 
 ENV X86_64_UNKNOWN_LINUX_MUSL_OPENSSL_INCLUDE_DIR=/usr/include/openssl/ 
 RUN RUSTFLAGS=-Clinker=musl-gcc /root/.cargo/bin/cargo build --release --target=x86_64-unknown-linux-musl --features vendored 
 RUN rm -f target/x86_64-unknown-linux-musl/release/deps/walk_away* 
 RUN rm src/main.rs 
 COPY ./src ./src 
 RUN RUSTFLAGS=-Clinker=musl-gcc /root/.cargo/bin/cargo build --release --target=x86_64-unknown-linux-musl --features vendored 
 
 FROM alpine:latest 
 COPY --from=cargo-build /walk_away/target/x86_64-unknown-linux-musl/release/walk_away . 
 COPY templates templates
 CMD ["./walk_away"]
```


I used an app from the team and i run it with a simple rust web server 
the default image have a big size `>1GB`, so the goal is to make an image with a small size


# The idea

i used the MUSl to help me to make the static linking of the project which means all the code and related stuff will be in a single binary.

the docker file id splitted into two sections : 

* The build section :
  all the work goes here, we build is with MUSL and generate a standalone binary

* The result section :
  now the binary dont need a full system to run it just work fine without the system libraries
  so the best choice to use is as a base image is the `alpine` image
  and finally we have a tiny image ready to be uploaded to dockerhub 


# make the build 

to make the build run this command : `docker build -f dockerfile.dev -t <image_name> .`

to run the app : `docker run -p 8000:8000 <image_name>



# devops integration

to integrate this image in the devops workflow you need to make some changes, here is the suggested a CI/CD file `.gitlab-ci.yml`
to make the build and the deployment to the gitlab registry : 


```yaml

.caching_rust: &caching_rust
    cache:
      paths:
        - .cargo/
        - .cache/sccache
        - target/x86_64-unknown-linux-musl/release/walk_away

stages:
    - build_binary
    - deps_audit 
    - build_docker


prepare_deps_for_cargo:
   stage: build_binary
   image: hatembt/rust-ci:latest
   <<: *caching_rust
   before_script:
       - pwd
       - export CARGO_HOME="${PWD}/.cargo"
       - echo $CARGO_HOME
       - export SCCACHE_DIR="${PWD}/.cache/sccache"
       - echo $SCCACHE_DIR
       - export PATH="/builds/isitcom_hugs_for_bugs_2019/dockerisation/.cargo/bin:$PATH"
       - export RUSTC_WRAPPER="$CARGO_HOME/bin/sccache"
       - echo $RUSTC_WRAPPER
       - cargo version
       - rustc --version
       - sccache --version

   script:
       -  cargo build --release --target=x86_64-unknown-linux-musl --features vendored
   cache:
     paths:
       - .cargo/
       - .cache/sccache
   artifacts:
     paths:
       - target/x86_64-unknown-linux-musl/release/walk_away
      
dependency_scanning:
  stage: deps_audit
  image: hatembt/rust-ci
  << : *caching_rust
  script:
    - cargo audit || true
    - cargo audit --json > cargo-audit-output.json || true
  artifacts:
    paths: ["cargo-audit-output.json"]

  cache:
    paths:
      - .cargo/
      - .cache/sccache

build_docker_image:
   stage: build_docker
   image: docker:latest
   << : *caching_rust
   services:
     - docker:dind
   script:
     - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
     - docker build -t registry.gitlab.com/isitcom_hugs_for_bugs_2019/dockerisation .  
     - docker push registry.gitlab.com/isitcom_hugs_for_bugs_2019/dockerisation:v1

```

this ci file has 3 main stags : 
* build_binary: 
     this stage will build the standalone binary  
*  deps_audit 
     this stage will scan all dependencies of any potentiel vulnerability issue 
*  build_docker
     this stage will get the binary ( as an artifact) and  put it into an apline image and push the image to the gitlab registry 

     this is the dockerfile used in the ci env.
```yaml
 FROM alpine:latest 
 COPY target/x86_64-unknown-linux-musl/release/walk_away . 
 COPY templates templates
 CMD ["./walk_away"]
```

# dockerfile generator 

i used a simple script to generate dockerfiles from rust projects to test your applications 