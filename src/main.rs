
#![feature(plugin, proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
extern crate rocket_contrib;

use std::collections::HashMap;


use rocket_contrib::templates::Template;
use std::path::{PathBuf, Path};
use rocket::response::NamedFile;

#[get("/templates/<file..>")]
pub fn file(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("templates/").join(file)).ok()
}

#[get("/")]
fn root() -> Template {

    let context = HashMap::<String, String>::new();
    Template::render("index", context )
}

fn main() {
    rocket::ignite()
        .mount("/", routes![
            file,
            root
        ])
        .attach(Template::fairing())
        .launch();
}
