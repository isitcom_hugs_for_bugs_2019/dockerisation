 FROM alpine:latest 
 COPY target/x86_64-unknown-linux-musl/release/walk_away . 
 COPY templates templates
 CMD ["./walk_away"]